module monitor_mqtt

go 1.15

require (
	github.com/eclipse/paho.mqtt.golang v1.2.0
	github.com/lib/pq v1.8.0
	golang.org/x/net v0.0.0-20200904194848-62affa334b73 // indirect
)
