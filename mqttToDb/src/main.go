package main

import (
	"database/sql"
	"fmt"
	"os"
	"os/signal"
	"strings"
	"syscall"

	MQTT "github.com/eclipse/paho.mqtt.golang"
	_ "github.com/lib/pq"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "1234"
	dbname   = "monitor"
)

var psqlInfo = fmt.Sprintf("host=%s port=%d user=%s "+
	"password=%s dbname=%s sslmode=disable",
	host, port, user, password, dbname)
var db, err = sql.Open("postgres", psqlInfo)

var f MQTT.MessageHandler = func(client MQTT.Client, msg MQTT.Message) {
	s := strings.Split(msg.Topic(), "/")
	go insertSensorData(s[1], s[2], msg.Payload())
}

func insertSensorData(sensor_type string, device_uuid string, sensor_data []byte) {
	sqlStatement := `INSERT INTO sensor_data (uuid, sensor_type, sensor_data) VALUES ($1, $2, $3);`
	_, err = db.Exec(sqlStatement, device_uuid, sensor_type, sensor_data)
	if err != nil {
		if err.Error() == "pq: insert or update on table \"sensor_data\" violates foreign key constraint \"sensor_data_uuid_fkey\"" {
			sqlStatement := `INSERT INTO device (uuid, fk_zone, device_name, device_status)
							VALUES ($1, $2, $3, $4) ON CONFLICT ON CONSTRAINT device_pkey DO NOTHING`
			_, err = db.Exec(sqlStatement, device_uuid, 3, "unknown", "unknown")
			checkErr(err)
			insertSensorData(sensor_type, device_uuid, sensor_data)
		} else if err.Error() == "pq: insert or update on table \"sensor_data\" violates foreign key constraint \"sensor_data_sensor_type_fkey\"" {
			sqlStatement := `INSERT INTO sensor_types (type_short, type_name)
							 VALUES ($1, $2) ON CONFLICT DO NOTHING`
			_, err = db.Exec(sqlStatement, sensor_type, "unknown")
			checkErr(err)
			insertSensorData(sensor_type, device_uuid, sensor_data)
		} else {
			checkErr(err)
		}
	}
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	// DB Connection check
	checkErr(err)
	defer db.Close()
	err = db.Ping()
	checkErr(err)

	c := make(chan os.Signal, 1)

	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	opts := MQTT.NewClientOptions().AddBroker("tcp://localhost:1883")
	opts.SetClientID("mqtt-to-postgresql")
	topic := "s/#"

	opts.OnConnect = func(c MQTT.Client) {
		if token := c.Subscribe(topic, 0, f); token.Wait() && token.Error() != nil {
			panic(token.Error())
		}
	}
	client := MQTT.NewClient(opts)
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}
	<-c
}
